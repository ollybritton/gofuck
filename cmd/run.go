// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"io/ioutil"

	"github.com/spf13/cobra"
	"gitlab.com/ollybritton/gofuck/brainfuck"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Execute a Brainfuck program",
	Long:  `Takes in a file containing Brainfuck code, executes it.`,

	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			fmt.Println("run requires a file to execute.\nUsage: gofuck run [file path]")
			return
		}

		filePath := args[0]

		if !brainfuck.IsValidFile(filePath) {
			panic("File specified is not a valid file. Valid files exist on the system and have an extension of .b, .bf or .brainfuck.")
		}

		fileBytes, err := ioutil.ReadFile(filePath)
		check(err)
		program := string(fileBytes)

		tapeSize, err := cmd.Flags().GetInt64("size")
		check(err)
		tape := brainfuck.CreateTape(int(tapeSize))

		tapePrint, err := cmd.Flags().GetBool("print")
		check(err)

		step, err := cmd.Flags().GetBool("step")
		check(err)
		plain, err := cmd.Flags().GetBool("plain")
		check(err)

		verbose, err := cmd.PersistentFlags().GetBool("verbose")
		check(err)

		delay, err := cmd.Flags().GetInt64("delay")
		check(err)

		options := brainfuck.RunOptions{
			Verbose:  verbose,
			Step:     step,
			FilePath: filePath,
			Delay:    delay,
			Plain:    plain,
		}

		tape = brainfuck.Run(program, tape, options)

		if tapePrint {
			fmt.Println(tape)
		}

	},
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func init() {
	rootCmd.AddCommand(runCmd)

	runCmd.Flags().Int64("size", 100, "Adjust the initial size of the tape.")
	runCmd.Flags().Int64P("delay", "d", 100, "Adjusts the delay that the program waits between steps in milliseconds when using verbose mode.")

	runCmd.Flags().BoolP("print", "p", false, "Print the contents of the tape after execution.")

	runCmd.Flags().BoolP("step", "s", false, "Wait for enter press after each instruction.")

	runCmd.Flags().Bool("plain", false, "Simply print the output of the program with no fluff.")
}
