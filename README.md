# gofuck
`gofuck` is a Brainfuck interpreter written in Go.

[![asciicast](https://asciinema.org/a/7PrTOsTsgCmfECMULweUV8gzM.svg)](https://asciinema.org/a/7PrTOsTsgCmfECMULweUV8gzM)

## Installation
`gofuck` can be installed using the command

```bash
go install gitlab.com/ollybritton/gofuck
```

After it has finished installing, you should have access to the `gofuck` command, assuming you have added the Go binary folder to your path. If you haven't this can be accomplished by running the following

```bash
export PATH=$PATH:$(go env GOPATH)/bin
```

## Usage
You can a brainfuck program like so:

```bash
gofuck run [path to file]
```

For example, to run the Hello World example program in `examples/`, you would do

```bash
gofuck run examples/helloWorld.bf
```

More help is avaliable through the `gofuck help` and `gofuck run help` commands.

## To Do
This is a list of things that I would still like to implement into the program.

* Accepting user input through a pipe (because that would definitely make the program useful)
* Optimise the program so that it is faster
* Make the user input only take one character at a time, without having to press enter