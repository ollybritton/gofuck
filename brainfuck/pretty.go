package brainfuck

import (
	"fmt"

	"github.com/buger/goterm"
	"github.com/fatih/color"
)

var (
	inactive           = color.New(color.FgCyan)
	active             = color.New(color.FgBlack).Add(color.BgCyan)
	headerRunning      = color.New(color.FgRed).Add(color.Underline)
	headerFinished     = color.New(color.FgGreen).Add(color.Underline)
	tapeColor          = color.New(color.FgWhite).Add(color.Underline)
	inputPromptColor   = color.New(color.FgYellow)
	inputResponseColor = color.New(color.FgGreen)
	outputColor        = color.New(color.FgGreen)
)

// PrintRunning will print a header for the program output whilst it is running, for example
// Running: examples/helloWorld.bf
func PrintRunning(filePath string) {
	headerRunning.Printf("Running: %s\n", filePath)
}

// PrintFinished will print a header for the program indicating has finished running, for example
// Finished: examples/helloWorld.bf
func PrintFinished(filePath string) {
	headerFinished.Printf("Finished: %s\n", filePath)
}

// PrintActive prints a program, with the active instruction highlighted.
func PrintActive(program string, activeIndex int) {
	beforeActive := program[0:activeIndex]
	atActive := string(program[activeIndex])
	afterActive := program[activeIndex+1:]

	inactive.Printf(beforeActive)
	active.Printf(atActive)
	inactive.Printf(afterActive + "\n")
}

// PrintTape will print the tape in a pretty, human-readable way.
func PrintTape(tape []byte) {
	tapeColor.Println(tape)
}

// PrintOutput will print the output in a pretty way.
func PrintOutput(output string) {
	outputColor.Printf("Output: %s\n", output)
}

// GetInput will take ask the user for input.
func GetInput() byte {
	var input string
	inputPromptColor.Printf("The program is expecting input: ")

	fmt.Scanf("%s", &input)

	inputResponseColor.Printf(input + "\n")

	return input[0]
}

// StepPrompt is the prompt that is displayed when stepping is enabled. It returns a string representing the action that the user wants to take.
func StepPrompt() {
	var input string

	inputPromptColor.Printf("Step (press enter) ")
	fmt.Scanf("%s", &input)
}

// ClearScreen will clear the terminal screen.
func ClearScreen() {
	goterm.Clear()
	goterm.MoveCursor(1, 1)
	goterm.Flush()
}
