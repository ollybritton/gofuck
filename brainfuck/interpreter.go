package brainfuck

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var (
	validExtensions = []string{".b", ".bf", ".brainfuck"}
)

// RunOptions defines options for how to run the Brainfuck program.
type RunOptions struct {
	Verbose  bool
	Step     bool
	Plain    bool
	Delay    int64
	FilePath string
}

// IsValidFile will check if the path given is a valid Brainfuck file.
func IsValidFile(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}

	var extension = filepath.Ext(path)

	for _, validExtension := range validExtensions {
		if validExtension == extension {
			return true
		}
	}

	return false
}

// CreateTape returns a byte array of a given length.
func CreateTape(length int) []byte {
	return make([]byte, length)
}

// FindMatchingCloseBracket will find the matching closing bracket in a program.
func FindMatchingCloseBracket(bracketLocation int, program string) int {
	var pointer = bracketLocation
	var bracketSum = 0

	var instructions = strings.Split(program, "")

	if string(instructions[bracketLocation]) != "[" {
		panic("Attempting to find close bracket on non-bracket instruction.")
	}

	for !(bracketSum == 0 && pointer != bracketLocation) {
		currentInstruction := instructions[pointer]

		switch currentInstruction {
		case "[":
			bracketSum++
		case "]":
			bracketSum--
		}

		pointer++
	}

	// Since the loop only stops after the pointer goes on to the next instruction, we need to subtract 1 so that the pointer actually points to the correct bracket.
	return pointer - 1
}

// FindMatchingOpenBracket will find the matching opening bracket in a program.
func FindMatchingOpenBracket(bracketLocation int, program string) int {
	var pointer = bracketLocation
	var bracketSum = 0

	var instructions = strings.Split(program, "")

	if string(instructions[bracketLocation]) != "]" {
		panic("Attempting to find open bracket on non-bracket instruction.")
	}

	for !(bracketSum == 0 && pointer != bracketLocation) {
		currentInstruction := instructions[pointer]

		switch currentInstruction {
		case "]":
			bracketSum++
		case "[":
			bracketSum--
		}

		pointer--
	}

	// Since the loop only stops after the pointer goes back onto the previous instruction, we need to add 1 so that the pointer actually points to the correct bracket.
	return pointer + 1
}

// Run will run a Brainfuck program using a given tape and returns the tape at the end of the execution.
func Run(program string, tape []byte, options RunOptions) []byte {
	var instructions = strings.Split(program, "")

	var pointer int
	var instructionIndex int

	var output string

	for instructionIndex < len(program) {

		if options.Verbose {
			ClearScreen()
			PrintRunning(options.FilePath)
			PrintActive(program, instructionIndex)
			PrintTape(tape)
		}

		var currentInstruction = instructions[instructionIndex]

		switch currentInstruction {
		case "+":
			// Add 1 to the current value.
			tape[pointer]++
		case "-":
			// Subtract 1 from the current value.
			tape[pointer]--
		case ">":
			// Move onto the next cell in memory.
			pointer++
		case "<":
			// Move back to the previous cell in memory.
			pointer--
		case "[":
			// If the value of the current cell is zero, skip to the closing bracket. If it is not, continue.
			if tape[pointer] == 0 {
				instructionIndex = FindMatchingCloseBracket(instructionIndex, program)
			}
		case "]":
			// If the value of the current cell if zero, move onto the next instruction. If not, jump back to opening bracket.
			if tape[pointer] != 0 {
				instructionIndex = FindMatchingOpenBracket(instructionIndex, program)
			}
		case ".":
			// Print UNICODE character represented by the current value in memory.
			output += string(tape[pointer])
		case ",":
			// Take in a character and replace the contents of the current cell with the UNICODE representation of that character.
			char := GetInput()
			tape[pointer] = char
		}

		instructionIndex++

		if options.Step {
			StepPrompt()
		}

		if options.Verbose {
			time.Sleep(time.Duration(options.Delay) * time.Millisecond)
		}

	}

	if !options.Plain {
		ClearScreen()
		PrintFinished(options.FilePath)
		PrintActive(program, len(program)-1)
		fmt.Println("")
		PrintOutput(output)
	} else {
		fmt.Println(output)
	}

	return tape
}
